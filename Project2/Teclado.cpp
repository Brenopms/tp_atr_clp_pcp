#define _WIN32_WINNT 0X0400
#define WIN32_LEAN_AND_MEAN 
#define _CRT_SECURE_CPP_OVERLOAD_STANDARD_NAMES 1
#define HAVE_STRUCT_TIMESPEC
#include <windows.h>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <conio.h>      // _getch
#include <errno.h>
#include<time.h>
#include <process.h>
#include <tchar.h>
#define	ESC			0x1B

#define _CHECKERROR	1	// Ativa fun��o CheckForError
#include "CheckForError.h"

//Objetos para a tarefa de leitura do teclado
HANDLE teclaPressionadaEvento;
HANDLE CLPSem;
HANDLE PCPSem;
HANDLE RetiradaMsgSem;
HANDLE GestaoProdSem;
HANDLE ExibeDadosSem;
HANDLE ESCEve;
HANDLE LimparGestao;

//Objetos que sinalizam as listas cheias 
HANDLE ListaCheia1;
HANDLE ListaCheia2;

//Variavel que captura a tecla pressionada
int capturaTeclaPressionada;

// DECLARACAO DO PROTOTIPO DE FUNCAO CORRESPONDENTES � THREAD SECUNDARIA
void* LerTeclado(void* arg);
void* EsperaListaCheia(void* arg);

//casting para o 3� e 6� parametros da fun��o beginthreadx
typedef unsigned (WINAPI *CAST_FUNCTION)(LPVOID);
typedef unsigned *CAST_LPDWORD;

//pipes
HANDLE hpipe2;
LPTSTR Pipe2 = "\\\\.\\pipe\\Pipe2"; //pipe usado para notificacao de limpeza da tela


int main()
{
	HANDLE hThreads3, hThreads4;
	DWORD dwThreadId3, dwThreadId4;

	//cria semaforos para o on-off das tarefas
	CLPSem = CreateSemaphore(NULL, 1, 1, "CLPSem");
	if (CLPSem == NULL)
		printf("Erro: %d na criacao do semaforo de sinalizacao de bloqueio da thread do CLP\n", GetLastError());
	
	PCPSem = CreateSemaphore(NULL, 1, 1, "PCPSem");
	if (PCPSem == NULL)
		printf("Erro: %d na criacao do semaforo de sinalizacao de bloqueio da thread do PCP\n", GetLastError());
	
	RetiradaMsgSem = CreateSemaphore(NULL, 1, 1, "RetiradaMsgSem");
	if (RetiradaMsgSem == NULL)
		printf("Erro: %d na criacao do semaforo de sinalizacao de bloqueio da thread de retirada de mensagens\n", GetLastError());
	
	GestaoProdSem = CreateSemaphore(NULL, 1, 1, "GestaoProdSem");
	if (GestaoProdSem == NULL)
		printf("Erro: %d na criacao do semaforo de sinalizacao de bloqueio da thread de gestao\n", GetLastError());
	
	ExibeDadosSem = CreateSemaphore(NULL, 1, 1, "ExibeDadosSem");
	if (ExibeDadosSem == NULL)
		printf("Erro: %d na criacao do semaforo de sinalizacao de bloqueio da thread de exibicao de dados\n", GetLastError());


	//Cria evento para sinalizacao da limpeza de console
	LimparGestao = CreateEvent(NULL, TRUE, FALSE, _T("LimparGestao"));
	CheckForError(LimparGestao);

	//Abre evento para o fim das tarefas
	ESCEve = OpenEvent(EVENT_ALL_ACCESS, TRUE, _T("ESCEvent"));
	CheckForError(ESCEve);

	//Abre os eventos para lista cheia 
	ListaCheia1 = OpenEvent(EVENT_ALL_ACCESS, TRUE, _T("Lista1"));
	CheckForError(ListaCheia1);

	ListaCheia2 = OpenEvent(EVENT_ALL_ACCESS, TRUE, _T("Lista2"));
	CheckForError(ListaCheia2);

	//Evento para sinalizar quando alguma tecla for digitada
	teclaPressionadaEvento = CreateEvent(NULL, FALSE, FALSE, "teclaPressionada");

	hpipe2 = CreateNamedPipe(           // cria servivor do pipe 2
		Pipe2,                          // nome do pipe 
		PIPE_ACCESS_DUPLEX,
		PIPE_TYPE_MESSAGE | PIPE_READMODE_MESSAGE,
		1,                             // instancias
		10000000,
		10000000,
		0,
		NULL);

	if (hpipe2 == NULL)
		printf("Erro: %d para criar o pipe 2\n", GetLastError());

	//aguarda pela conexao do cliente ao pipe 2
	ConnectNamedPipe(hpipe2, NULL); 

	//prepara o cliente do pipe 2 para receber mensagens
	WriteFile(hpipe2, "oi", sizeof("oi"), NULL, 0); 

	//cria thread respons�vel por ler entradas no teclado
	hThreads3 = (HANDLE)_beginthreadex(NULL, 0, (CAST_FUNCTION)LerTeclado, (LPVOID)0, 0, (CAST_LPDWORD)&dwThreadId3);

	if (hThreads3)
		printf("Thread de leitura do teclado criada com sucesso\n", dwThreadId3);
	else
		printf("Erro na criacao da thread %d referente � leitura do teclado!\n", 3);

	//cria thread respons�vel por sinalizar lista cheia
	hThreads4 = (HANDLE)_beginthreadex(NULL, 0, (CAST_FUNCTION)EsperaListaCheia, (LPVOID)0, 0, (CAST_LPDWORD)&dwThreadId4);

	if (hThreads4)
		printf("Thread de espera da lista cheia criada com sucesso!\n", dwThreadId4);
	else
		printf("Erro na criacao da thread %d referente a espera da lista cheia!\n", 4);

	//Interface de intrucoes para  o usuario
	printf("\n----------------Ola usuario! Escolha uma opcao!----------------\n");
	printf("------------Aperte P para bloquear a tarefa do CLP-------------\n");
	printf("------------Aperte S para bloquear a tarefa do PCP-------------\n");
	printf("----Aperte R para bloquear a tarefa de retirada de mensagens----\n");
	printf("------Aperte G para bloquear a tarefa de gestao de producao-----\n");
	printf("------Aperte D para bloquear a tarefa de exibicao de dados------\n");
	printf("---Aperte C para limpar a tela da tarefa de gestao de producao---\n");
	printf("------------Aperte ESC para encerrar todas as tarefas------------\n\n");

	//espera uma tecla ser digitada
	do {
		capturaTeclaPressionada = _getwch();
		SetEvent(teclaPressionadaEvento);
	} while (capturaTeclaPressionada != ESC);

	WaitForSingleObject(hThreads3, INFINITE); //espera pelo termino da thread
	WaitForSingleObject(hThreads4, INFINITE); //espera pelo termino da thread

	//fecha os handles usados
	CloseHandle(hThreads3);
	CloseHandle(ESCEve);
	CloseHandle(PCPSem);
	CloseHandle(CLPSem);
	CloseHandle(GestaoProdSem);
	CloseHandle(ExibeDadosSem);
	CloseHandle(RetiradaMsgSem);
	CloseHandle(teclaPressionadaEvento);

	return 0;
}  // main

//tarefa da leitura do teclado
void *LerTeclado(void *arg) {
	DWORD status;
	int p = 0;
	int s = 0;
	int r = 0;
	int g = 0;
	int d = 0;
	int c = 0;
	char clear[10] = "clear";

	while (true) {
		status = WaitForSingleObject(teclaPressionadaEvento, INFINITE);

		//Notifica a tarefa de Leitura do CLP que esta deve se bloquear ou retomar sua execucao 
		if (capturaTeclaPressionada == 'p') {
			if (p == 0) {
				WaitForSingleObject(CLPSem, INFINITE);
				p = 1;
				printf("Tarefa do CLP bloqueada pelo usuario\n");
			}
			else {
				ReleaseSemaphore(CLPSem, 1, NULL);
				p = 0;
				printf("Tarefa do CLP desbloqueada pelo usuario\n");
			}
		}
		//Notifica a tarefa de Leitura do PCP que esta deve se bloquear ou retomar sua execucao 
		else if (capturaTeclaPressionada == 's') {
			if (s == 0) {
				WaitForSingleObject(PCPSem, INFINITE);
				s = 1;
				printf("Tarefa do PCP bloqueada pelo usuario\n");
			}
			else {
				ReleaseSemaphore(PCPSem, 1, NULL);
				s = 0;
				printf("Tarefa do PCP desbloqueada pelo usuario\n");
			}
		}
		//Notifica a tarefa de retirada de mensagens que esta deve se bloquear ou retomar sua execucao 
		else if (capturaTeclaPressionada == 'r') {
			if (r == 0) {
				WaitForSingleObject(RetiradaMsgSem, INFINITE);
				r = 1;
				printf("Tarefa da captura de mensagens bloqueada pelo usuario\n");
			}
			else {
				ReleaseSemaphore(RetiradaMsgSem, 1, NULL);
				r = 0;
				printf("Tarefa da captura de mensagens desbloqueada pelo usuario\n");
			}
		}
		//Notifica a tarefa de gestao de producao que esta deve se bloquear ou retomar sua execucao 
		else if (capturaTeclaPressionada == 'g') {
			if (g == 0) {
				WaitForSingleObject(GestaoProdSem, INFINITE);
				g = 1;
				printf("Tarefa da gestao de producao bloqueada pelo usuario\n");
			}
			else {
				ReleaseSemaphore(GestaoProdSem, 1, NULL);
				g = 0;
				printf("Tarefa da gestao de producao desbloqueada pelo usuario\n");
			}
		}
		//Notifica a tarefa de Exibicao de dados de processo que esta deve se bloquear ou retomar sua execucao 
		else if (capturaTeclaPressionada == 'd') {
			if (d == 0) {
				WaitForSingleObject(ExibeDadosSem, INFINITE);
				d = 1;
				printf("Tarefa que exibe os dados bloqueada pelo usuario\n");
			}
			else {
				ReleaseSemaphore(ExibeDadosSem, 1, NULL);
				d = 0;
				printf("Tarefa que exibe os dados desbloqueada pelo usuario\n");
			}
		}
		//Notifica a tarefa de limpeza de tela que esta deve se bloquear ou retomar sua execucao 
		else if (capturaTeclaPressionada == 'c') {
			SetEvent(LimparGestao);
			WriteFile(hpipe2, clear, 10, NULL, 0);
			printf("Tarefa de gestao sendo limpa\n");
		}

		//Notifica que todas as tarefas devem encerrar sua execucao
		else if (capturaTeclaPressionada == ESC) {
			SetEvent(ESCEve); 
			printf("O processo esta sendo encerrado\n");
			_endthreadex(0);
		}

		else
			printf("Tecla invalida\n");

	}
	_endthreadex(0);
	return 0;
}

//thread que espera por sinalizacao de lista cheia
void *EsperaListaCheia(void *arg) {
	int tipo;
	DWORD status;
	HANDLE eventos[3] = { ESCEve, ListaCheia1, ListaCheia2 };

	do {
		//espera EESC ou listas cheias
		status = WaitForMultipleObjects(3, eventos, FALSE, INFINITE);
		tipo = status - WAIT_OBJECT_0;

		if (tipo == 0) {
			break;
		}

		else if (tipo == 1)
			printf("Lista circular 1 cheia! Tarefa do CLP/PCP bloqueada\n");
		else if (tipo == 2)
			printf("Lista circular 2 cheia! Tarefa de retirada de mensagens bloqueada\n");
		else
			printf("ERROR\n");
	} while (true);

	_endthreadex(0);
	return 0;
}