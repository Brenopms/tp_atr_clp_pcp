#define _WIN32_WINNT 0X0400
#define WIN32_LEAN_AND_MEAN 
#define _CRT_SECURE_CPP_OVERLOAD_STANDARD_NAMES 1
#define HAVE_STRUCT_TIMESPEC
#include <windows.h>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <conio.h>      // _getch
#include <errno.h>
#include<time.h>
#include <process.h>
#include <tchar.h>
#define	ESC			0x1B

#define _CHECKERROR	1	// Ativa fun��o CheckForError
#include "CheckForError.h"

// DECLARACAO DO PROTOTIPO DE FUNCAO CORRESPONDENTES � THREAD SECUNDARIA
void* Gestao(void* arg);

//Objetos para a tarefa de leitura do teclado
HANDLE GestaoProdSem;
HANDLE ESCEve;
HANDLE LimparGestao;

//casting para o 3� e 6� parametros da fun��o beginthreadx
typedef unsigned (WINAPI *CAST_FUNCTION)(LPVOID);
typedef unsigned *CAST_LPDWORD;

//pipes
HANDLE hpipe1;
HANDLE hpipe2;
LPTSTR Pipe1 = "\\\\.\\pipe\\Pipe1"; //pipe que sera usado para mensagem de ordem de producao
LPTSTR Pipe2 = "\\\\.\\pipe\\Pipe2"; //pipe usado para notificacao de limpeza da tela

int main()
{
	HANDLE hThreads5;
	DWORD dwThreadId5;
	int status, tipo;
	char clear[10] = "";
	char msg[10] = "";

	//abre semaforo para o on-off da tarefa de gestao
	GestaoProdSem = CreateSemaphore(NULL, 1, 1, "GestaoProdSem");
	int error = GetLastError();
	if (GestaoProdSem == NULL)
		printf("Erro: %d na criacao do semaforo de sinalizacao de bloqueio da thread de gestao\n", GetLastError());

	//abre evento para o fim das tarefas
	ESCEve = OpenEvent(EVENT_ALL_ACCESS, TRUE, _T("ESCEvent"));
	int error1 = GetLastError();
	CheckForError(ESCEve);

	//Abre o evento para limpeza do console
	LimparGestao = OpenEvent(EVENT_ALL_ACCESS, TRUE, _T("LimparGestao"));
	if (LimparGestao == NULL)
		int errorLimpar = GetLastError();

	//aguarda uma instancia do pipe
	WaitNamedPipe(Pipe1, NMPWAIT_WAIT_FOREVER); 

	hpipe1 = CreateFile(  //abre o pipe1
		Pipe1,            // nome do pipe 
		GENERIC_READ |    // acesso para leitura e escrita 
		GENERIC_WRITE,
		0,               // sem compartilhamento 
		NULL,            // lpSecurityAttributes
		OPEN_EXISTING,   // dwCreationDistribution 
		0,               // dwFlagsAndAttributes 
		NULL);           // hTemplate

	if (hpipe1 == INVALID_HANDLE_VALUE)
		printf("erro para criar o pipe 1: %d\n", GetLastError());

	//aguarda uma instancia do pipe
	WaitNamedPipe(Pipe2, NMPWAIT_WAIT_FOREVER);

	hpipe2 = CreateFile(  //abre o pipe 2
		Pipe2,            // nome do pipe 
		GENERIC_READ |    // acesso para leitura e escrita 
		GENERIC_WRITE,
		0,               // sem compartilhamento 
		NULL,            // lpSecurityAttributes
		OPEN_EXISTING,   // dwCreationDistribution 
		0,               // dwFlagsAndAttributes 
		NULL);           // hTemplate

	if (hpipe2 == INVALID_HANDLE_VALUE)
		printf("erro para criar o pipe 2: %d\n", GetLastError());

	//se prepara para receber mensagens do servidor do pipe 1
	ReadFile(hpipe1, msg, 100, NULL, 0); 

	//se prepara para receber mensagens do servidor do pipe 2
	ReadFile(hpipe2, msg, 100, NULL, 0); 


	//cria thread respons�vel pela gestao de dados do processo
	hThreads5 = (HANDLE)_beginthreadex(NULL, 0, (CAST_FUNCTION)Gestao, (LPVOID)0, 0, (CAST_LPDWORD)&dwThreadId5);

	if (hThreads5)
		printf("Thread de de gestao da producao criada com sucesso\n");
	else
		printf("Erro na criacao da thread %d referente � gestao da producao!\n", 5);
	HANDLE eventos[2] = { ESCEve, LimparGestao };

	do {
		//Espera pela sinalizacao do ESC ou da limpeza do console
		status = WaitForMultipleObjects(2, eventos, FALSE, INFINITE);
		tipo = status - WAIT_OBJECT_0;
		if (tipo == 0)
			break;

		else if (tipo == 1) {
			ResetEvent(LimparGestao);
			ReadFile(hpipe2, clear, 10, NULL, 0);
			system("CLS");
		}
		else
			printf("ERRO \n");
	} while (true);

	//aguarda pelo termino da tarefa
	WaitForSingleObject(hThreads5, INFINITE); 

	//fecha os handles usados
	CloseHandle(hpipe1);
	CloseHandle(hpipe2);
	CloseHandle(GestaoProdSem);
	CloseHandle(ESCEve);
	CloseHandle(hThreads5);

	return 0;
}  // main

//Thread que mostra as mensagens no console do PCP
void *Gestao(void *arg) {
	int i, tipo, status;
	int count = 0;
	//define os campos das mensagens
	char msg[200] = "";
	char result[200] = "";
	char hora[20] = "";
	char op1[20] = "";
	char slot1[10] = "";
	char op2[20] = "";
	char slot2[10] = "";
	char op3[20] = "";
	char slot3[10] = "";

	HANDLE waitGestao[2] = { ESCEve, GestaoProdSem };

	do {
		status = WaitForMultipleObjects(2, waitGestao, FALSE, INFINITE); // sincroniza com o teclado
		tipo = status - WAIT_OBJECT_0;
		if (tipo == 0)
			break;

		else {
			//Espera pela escrita do pipe1
			ReadFile(hpipe1, msg, 100, NULL, 0);

			//Formata as mensagens
			for (i = 0; i < 8; i++)
				hora[i] = msg[i + 5];

			for (i = 0; i < 8; i++)
				op1[i] = msg[i + 14];

			for (i = 0; i < 4; i++)
				slot1[i] = msg[i + 23];

			for (i = 0; i < 8; i++)
				op2[i] = msg[i + 28];

			for (i = 0; i < 4; i++)
				slot2[i] = msg[i + 37];

			for (i = 0; i < 8; i++)
				op3[i] = msg[i + 42];

			for (i = 0; i < 4; i++)
				slot3[i] = msg[i + 51];			

			sprintf_s(result, "NSEQ:%i REF:%s OP1:%s [%s] OP2:%s [%s] OP3:%s [%s]", count, hora, op1, slot1, op2, slot2, op3, slot3);
			printf("%s\n", result);
			count++;
			ReleaseSemaphore(GestaoProdSem, 1, NULL);
		}


	} while (true); // Esc foi escolhido
	printf("Thread de gestao da producao sendo encerrada\n");
	_endthreadex(0);
	return(0);
}


