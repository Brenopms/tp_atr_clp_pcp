#define _WIN32_WINNT 0X0400
#define WIN32_LEAN_AND_MEAN 
#define _CRT_SECURE_CPP_OVERLOAD_STANDARD_NAMES 1
#define HAVE_STRUCT_TIMESPEC
#include <windows.h>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <Winbase.h>
#include <conio.h>      // _getchw
#include <errno.h>
#include<time.h>
#include <process.h>
#include <tchar.h>
#define	ESC			0x1B


#define _CHECKERROR	1	// Ativa fun��o CheckForError
#include "CheckForError.h"

// Declara��o dos objetos da primeira lista circular
HANDLE hVazias1; //posi��es vazias no buffer
HANDLE hOcupadas1; //posi��es ocupadas no buffer
HANDLE hMutex1; //mutex para garantir a exclus�o m�tua no buffer

#define n1 200
char lista_circular1[n1][60];
int pos_entrada1 = 0;
int pos_retirada1 = 0;

// Declara��o dos objetos da segunda lista circular
HANDLE hVazias2; //posi��es vazias no buffer
HANDLE hOcupadas2; //posi��es ocupadas no buffer

#define n2 100
char lista_circular2[n2][55];
int pos_entrada2 = 0;
int pos_retirada2 = 0;

//Objetos para a tarefa de leitura do teclado
HANDLE CLPSem;
HANDLE PCPSem;
HANDLE RetiradaMsgSem;
HANDLE ExibeDadosSem;
HANDLE ESCEve;

//Objetos para sinalizacao de lista cheia
HANDLE ListaCheia1;
HANDLE ListaCheia2;

// DECLARACAO DO PROTOTIPO DE FUNCAO CORRESPONDENTES �S THREADS SECUNDARIAS
void* LeituraCLP(void* arg);
void* LeituraPCP(void* arg);
void* CapturaMsg(void* arg);
void* Exibicao(void* arg);

//pipes
HANDLE hpipe1;
LPTSTR Pipe1 = "\\\\.\\pipe\\Pipe1"; //pipe que sera usado para mensagem de ordem de producao

//casting para o 3� e 6� parametros da fun��o beginthreadx
typedef unsigned (WINAPI *CAST_FUNCTION)(LPVOID);
typedef unsigned *CAST_LPDWORD;

WORD retorno0, retorno1;
SYSTEMTIME tempo;
HANDLE hEvent;

int main()
{
	HANDLE hThreads0, hThreads1, hThreads2, hThreads4;
	DWORD dwThreadId0, dwThreadId1, dwThreadId2, dwThreadId4;
	int i, status, process1, process2;

	//cria evento que nunca ir� ficar sinalizado
	hEvent = CreateEvent(NULL, TRUE, FALSE, "EvTimeOut");
	CheckForError(hEvent);

	// parametros para a funcao createprocess
	STARTUPINFO si;
	PROCESS_INFORMATION pi;

	ZeroMemory(&si, sizeof(si));
	si.cb = sizeof(si);
	ZeroMemory(&pi, sizeof(pi));

	//inicializa objetos do primeiro buffer
	hMutex1 = CreateMutex(NULL, FALSE, NULL);
	CheckForError(hMutex1);
	hVazias1 = CreateSemaphore(NULL, n1, n1, NULL);
    CheckForError(hVazias1);
	hOcupadas1 = CreateSemaphore(NULL, 0, n1, NULL);
	CheckForError(hOcupadas1);

	//inicializa objetos do segundo buffer
	hVazias2 = CreateSemaphore(NULL, n2, n2, NULL);
	CheckForError(hVazias2);
	hOcupadas2 = CreateSemaphore(NULL, 0, n2, NULL);
	CheckForError(hOcupadas2);

	//abre semaforos para o on-off das tarefas
	CLPSem = CreateSemaphore(NULL, 1, 1, "CLPSem");
	if (CLPSem == NULL)
		printf("Erro: %d na abertura do semaforo de sinalizacao de bloqueio da thread do CLP\n", GetLastError());

	PCPSem = CreateSemaphore(NULL, 1, 1, "PCPSem");
	if (PCPSem == NULL)
		printf("Erro: %d na abertura do semaforo de sinalizacao de bloqueio da thread do PCP\n", GetLastError());

	RetiradaMsgSem = CreateSemaphore(NULL, 1, 1, "RetiradaMsgSem");
	if (RetiradaMsgSem == NULL)
		printf("Erro: %d na abertura do semaforo de sinalizacao de bloqueio da thread de retirada de mensagens\n", GetLastError());

	ExibeDadosSem = CreateSemaphore(NULL, 1, 1, "ExibeDadosSem");
	if (ExibeDadosSem == NULL)
		printf("Erro: %d na abertura do semaforo de sinalizacao de bloqueio da thread de exibicao de dados\n", GetLastError());

	//abre evento para o fim das threads
	ESCEve = CreateEvent(NULL, TRUE, FALSE, _T("ESCEvent"));
	CheckForError(ESCEve);

	//Cria os eventos de sinalizacao das listas cheias 
	ListaCheia1 = CreateEvent(NULL, FALSE, FALSE, _T("Lista1"));
	ListaCheia2 = CreateEvent(NULL, FALSE, FALSE, _T("Lista2"));


	process1 = CreateProcess(                       // create process
		"..\\Debug\\Teclado.exe",                   // Nome
		NULL,	                                    // linha de comando: Passa id do processo como par�metro
		NULL,	                                    // atributos de seguran�a: Processo
		NULL,	                                    // atributos de seguran�a: Thread
		FALSE,	                                    // heran�a de handles
		NORMAL_PRIORITY_CLASS | CREATE_NEW_CONSOLE,	// CreationFlags
		NULL,	                                    // lpEnvironment
		"..\\Debug",                                // Nome
		&si,			                            // lpStartUpInfo
		&pi);	                                    // lpProcessInformation
	
	if (process1 == 0)
		printf("Erro no createprocess do Teclado : %d", GetLastError());
	else
		printf("Processo do teclado criado com sucesso\n");

	process2 = CreateProcess(                       // create process
		"..\\Debug\\Gestao.exe",                    // Nome
		NULL,	                                    // linha de comando: Passa id do processo como par�metro
		NULL,	                                    // atributos de seguran�a: Processo
		NULL,	                                    // atributos de seguran�a: Thread
		FALSE,	                                    // heran�a de handles
		NORMAL_PRIORITY_CLASS | CREATE_NEW_CONSOLE,	// CreationFlags
		NULL,	                                    // lpEnvironment
		"..\\Debug",                                // Nome
		&si,			                            // lpStartUpInfo
		&pi);	                                    // lpProcessInformation

	if (process2 == 0)
		printf("Erro no createprocess da tarefa de gestao : %d", GetLastError());
	else
		printf("Processo de gestao da producao criado com sucesso\n");

	hpipe1 = CreateNamedPipe(           // cria servivor do pipe 1
		Pipe1,                  // nome do pipe 
		PIPE_ACCESS_DUPLEX,
		PIPE_TYPE_MESSAGE | PIPE_READMODE_MESSAGE,
		1,                             // instancias
		10000000,
		10000000,
		0,
		NULL);

	if (hpipe1 == NULL)
		printf("Erro: %d para criar o pipe 1\n", GetLastError());

	ConnectNamedPipe(hpipe1, NULL); //aguarda pela conexao do cliente ao pipe

	WriteFile(hpipe1, "oi", sizeof("oi"), NULL, 0); //prepara o cliente do pipe 1 para receber mensagens

	//cria thread respons�vel pela leitura do CLP
	hThreads0 = (HANDLE)_beginthreadex(NULL, 0, (CAST_FUNCTION)LeituraCLP, (LPVOID)0, 0, (CAST_LPDWORD)&dwThreadId0);

	if (hThreads0)
		printf("Thread da leitura do CLP criada com sucesso\n");
	else
		printf("Erro na criacao da thread %d referente a leitura do CLP!\n", 0);

	//cria thread respons�vel pela leitura do PCP
	hThreads1 = (HANDLE)_beginthreadex(NULL, 0, (CAST_FUNCTION)LeituraPCP, (LPVOID)0, 0, (CAST_LPDWORD)&dwThreadId1);

	if (hThreads1)
		printf("Thread da leitura do PCP criada com sucesso\n");
	else
		printf("Erro na criacao da thread %d referente a leitura do PCP!", 1);

	//cria thread respons�vel pela captura das mensagens
	hThreads2 = (HANDLE)_beginthreadex(NULL, 0, (CAST_FUNCTION)CapturaMsg, (LPVOID)0, 0, (CAST_LPDWORD)&dwThreadId2);

	if (hThreads2)
		printf("Thread da captura de mensagens criada com sucesso\n");
	else
		printf("Erro na criacao da thread %d referente � captura de mensagens!\n", 2);

	//cria thread respons�vel pela exibicao de dados do processo
	hThreads4 = (HANDLE)_beginthreadex(NULL, 0, (CAST_FUNCTION)Exibicao, (LPVOID)0, 0, (CAST_LPDWORD)&dwThreadId4);

	if (hThreads4)
		printf("Thread da exibicao de dados do processo criada com sucesso\n");
	else
		printf("Erro na criacao da thread %d referente � exibicao de dados do processo!\n", 4);

	//aguarda pelo termino das threads
	WaitForSingleObject(hThreads0, INFINITE);
	WaitForSingleObject(hThreads1, INFINITE);
	WaitForSingleObject(hThreads2, INFINITE);
	WaitForSingleObject(hThreads4, INFINITE);

	//fecha os handles usados
	CloseHandle(hThreads0);
	CloseHandle(hThreads1);
	CloseHandle(hThreads2);
	CloseHandle(hThreads4);
	CloseHandle(hpipe1);
	CloseHandle(CLPSem);
	CloseHandle(PCPSem);
	CloseHandle(ExibeDadosSem);
	CloseHandle(RetiradaMsgSem);
	CloseHandle(ESCEve);
	CloseHandle(hEvent);
	CloseHandle(hVazias1);
	CloseHandle(hOcupadas1);
	CloseHandle(hMutex1);
	CloseHandle(hVazias2);
	CloseHandle(hOcupadas2);

	return 0;
}  // main

//fun��o que gera numeros inteiros aleatorios
int gerarNumeroAleatorioInteiro(int digit) { 
	int y = pow(10, digit);
	int x = rand() % y;
	return x;
}

//fun��o que gera numeros reais aleatorios 
float gerarNumeroAleatorioReal(int digit) { 
	float y = pow(10.0, digit - 2);
	float x = fmod(rand(), (y));
	float z = (rand() % 10) / 10.0;
	x = x + z;
	return x;
}

//fun��o que gera sequencias alfanumericas aleatorias
void gerarAlfaNumAleatorio(char *alfa, int len) { 
	char charSet[] = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	for (int i = 0; i < len; i++) {
		alfa[i] = charSet[rand() % (sizeof(charSet) - 1)];
	}
	alfa[len] = 0;
}

//funcao que identifica se e uma mensagem do CLP ou do PCP
int IdentificarTipoMsg(char* msg) {
	if (msg[7] == ':') {
		return 1; //mensagem do PCP
	}
	else {
		return 0; //mensagem do CLP
	}
}

// THREADS SECUNDARIAS

//Leitura do CLP
void *LeituraCLP(void *arg)
{
	int i = 0, ret, tipo, status;
	srand(time(NULL));
	HANDLE waitCLP[2] = { ESCEve, CLPSem };
	HANDLE waitVazias[2] = { ESCEve, hVazias1 };
	HANDLE waitMutex[2] =  { ESCEve, hMutex1 };

	do {
	    status = WaitForMultipleObjects(2, waitCLP, FALSE, INFINITE); // sincroniza com o teclado
		tipo = status - WAIT_OBJECT_0;
		if (tipo == 0)
			break;
		else {
			//criacao dos campos das mensagens
			int NSEQ = i % 1000000;
			i++;
			float TZONA1 = gerarNumeroAleatorioReal(6);
			float TZONA2 = gerarNumeroAleatorioReal(6);
			float TZONA3 = gerarNumeroAleatorioReal(6);
			float VOLUME = gerarNumeroAleatorioReal(5);
			float PRESSAO = gerarNumeroAleatorioReal(5);
			int TEMPO = gerarNumeroAleatorioInteiro(4);

			//obtem a hora corrente
			GetLocalTime(&tempo); 

			//formata��o da mensagem
			char mensagem[54];
			snprintf(mensagem, 54, "%06d/%06.1f/%06.1f/%06.1f/%05.1f/%05.1f/%04d/%02d:%02d:%02d", NSEQ, TZONA1, TZONA2, TZONA3, VOLUME, PRESSAO, TEMPO, tempo.wHour, tempo.wMinute, tempo.wSecond);

			status = WaitForMultipleObjects(2, waitVazias, FALSE, 0);
			tipo = status - WAIT_OBJECT_0;
			if (tipo == 0)
				break;
			else {
				//Checa se a lista esta cheia
				if (status == WAIT_TIMEOUT) {
					// printf("Lista circular 1 cheia! Tarefa do CLP bloqueada\n");
					SetEvent(ListaCheia1);

					//Espera abrir uma espaco na lista 1
					status = WaitForMultipleObjects(2, waitVazias, FALSE, INFINITE); 
					tipo = status - WAIT_OBJECT_0;
					if (tipo == 0)
						break;
				}

				//salva mensagem no buffer
				for (int cnt = 0; cnt < 54; cnt++) {

					lista_circular1[pos_entrada1][cnt] = mensagem[cnt];

				}

				status = WaitForMultipleObjects(2, waitMutex, FALSE, INFINITE); // sincroniza com o teclado
				tipo = status - WAIT_OBJECT_0;
				if (tipo == 0)
					break;
				else {
					pos_entrada1 = (pos_entrada1 + 1) % n1;
					ReleaseSemaphore(hOcupadas1, 1, NULL);
					ReleaseSemaphore(CLPSem, 1, NULL);
					ReleaseMutex(hMutex1);

					retorno0 = WaitForSingleObject(hEvent, 500);
					if (retorno0 == WAIT_TIMEOUT)
						continue;
					else
						printf("erro na temporiza��o do CLP");
				}
				//espera por meio segundo o evento que nunca ira sinalizar e depois prossegue
			}
		}
		

	} while (true); // Esc foi escolhido
	printf("Thread CLP sendo encerrada\n");
	_endthreadex(0);
	return(0);
}

//leitura PCP
void *LeituraPCP(void *arg)
{
	int status,tipo,i=0;
	srand(time(NULL));
	HANDLE objetos[2] = { ESCEve, PCPSem };
	HANDLE waitVazias[2] = { ESCEve, hVazias1 };
	HANDLE waitMutex[2] = { ESCEve, hMutex1 };

	do {
		status = WaitForMultipleObjects(2, objetos, FALSE, INFINITE); // sincroniza com o teclado
		tipo = status - WAIT_OBJECT_0;
		if (tipo == 0)
			break;
		else {
			//criacao dos campos das mensagens
			int NSEQ = i % 10000;
			i++;
			char OP1[8];
			gerarAlfaNumAleatorio(OP1, 8);

			int SLOT1 = gerarNumeroAleatorioInteiro(4);

			char OP2[8];
			gerarAlfaNumAleatorio(OP2, 8);

			int SLOT2 = gerarNumeroAleatorioInteiro(4);

			char OP3[8];
			gerarAlfaNumAleatorio(OP3, 8);

			int SLOT3 = gerarNumeroAleatorioInteiro(4);

			//gera um horario aleatorio
			int horas = (gerarNumeroAleatorioInteiro(2)) % 24;
			int minutos = (gerarNumeroAleatorioInteiro(2)) % 60;
			int segundos = (gerarNumeroAleatorioInteiro(2)) % 60;

			//formata��o da mensagem
			char mensagem[56] = "";
			snprintf(mensagem, 56, "%04d|%02d:%02d:%02d|%s|%04d|%s|%04d|%s|%04d|", NSEQ, horas, minutos, segundos, OP1, SLOT1, OP2, SLOT2, OP3, SLOT3);

			status = WaitForMultipleObjects(2, waitVazias, FALSE, 0);
			tipo = status - WAIT_OBJECT_0;
			if (tipo == 0)
				break;
			else {
				if (status == WAIT_TIMEOUT) {
					//Checa se a lista esta cheia
					//printf("Lista circular 1 cheia! Tarefa do PCP bloqueada\n");

					//Espera por um espaco na lista 1
					status = WaitForMultipleObjects(2, waitVazias, FALSE, INFINITE);
					tipo = status - WAIT_OBJECT_0;
					if (tipo == 0)
						break;
				}

				//salva mensagem no buffer
				for (int cnt = 0; cnt < 54; cnt++) {

					lista_circular1[pos_entrada1][cnt] = mensagem[cnt];

				}

				status = WaitForMultipleObjects(2, waitMutex, FALSE, INFINITE); // sincroniza com o teclado
				tipo = status - WAIT_OBJECT_0;
				if (tipo == 0)
					break;
				else {
					pos_entrada1 = (pos_entrada1 + 1) % n1;
					ReleaseSemaphore(hOcupadas1, 1, NULL);
					ReleaseMutex(hMutex1);

					//gera tempo aleat�rio entre 1 e 5 segundos para as mensagens
					int tempo_aleatorio = (gerarNumeroAleatorioInteiro(1)) % 5 + 1;

					ReleaseSemaphore(PCPSem, 1, NULL);

					//espera por esse tempo aleatorio usando o evento que nunca ira sinalizar
					retorno1 = WaitForSingleObject(hEvent, tempo_aleatorio * 1000);
					if (retorno1 == WAIT_TIMEOUT)
						continue;
					else
						printf("erro na temporiza��o do PCP");
				}
			}
		}
	} while (true);// Esc foi escolhido
	printf("Thread PCP sendo encerrada\n");
	_endthreadex(0);
	return(0);
}

//tarefa de captura de mensagens
void *CapturaMsg(void *arg) {
	char msg[100];
	int status, tipo, escrita;
	HANDLE waitRetiradaMsg[2] = { ESCEve, RetiradaMsgSem };
	HANDLE waitVazias[2] = { ESCEve, hVazias2 };
	HANDLE waitOcupados[2] = { ESCEve, hOcupadas1 };

	do {
		status = WaitForMultipleObjects(2, waitRetiradaMsg, FALSE, INFINITE); // sincroniza com o teclado
		tipo = status - WAIT_OBJECT_0;
	    if (tipo == 0)
			break;
		else {
			status = WaitForMultipleObjects(2, waitOcupados, FALSE, INFINITE); // sincroniza com o teclado
			tipo = status - WAIT_OBJECT_0;
			if (tipo == 0)
				break;
			else {
				if (IdentificarTipoMsg(lista_circular1[pos_retirada1]) == 0) {

					status = WaitForMultipleObjects(2, waitVazias, FALSE, 0);
					tipo = status - WAIT_OBJECT_0;
					if (tipo == 0)
						break;
					else {
						//checa se a lista 2 esta cheia 
						if (status == WAIT_TIMEOUT) {
							//printf("Lista circular 2 cheia! Tarefa de retirada de mensagens bloqueada\n");
							SetEvent(ListaCheia2);

							//espera por um espaco na lista 2
							status = WaitForMultipleObjects(2, waitVazias, FALSE, INFINITE);
							tipo = status - WAIT_OBJECT_0;
							if (tipo == 0)
								break;
						}

						//coloca as mensagens em uma segunda lista circular
						for (int cnt = 0; cnt < 55; cnt++) {
							lista_circular2[pos_entrada2][cnt] = lista_circular1[pos_retirada1][cnt];
						}

						pos_entrada2 = (pos_entrada2 + 1) % n2;
						ReleaseSemaphore(hOcupadas2, 1, NULL);
					}
				}
				else {
					//envia as mensagens diretamente para a tarefa de gest�o de produ��o
					for (int i = 0; i < 100; i++) {
						msg[i] = lista_circular1[pos_retirada1][i];
					}
					WriteFile(hpipe1, msg, sizeof(msg), NULL, 0);
				}
				pos_retirada1 = (pos_retirada1 + 1) % n1;
				ReleaseSemaphore(hVazias1, 1, NULL);
				ReleaseSemaphore(RetiradaMsgSem, 1, NULL);
			}
		}
			
	} while (true); //ESC escolhido

	printf("Thread da retirada de mensagens sendo ecerrada!");
	_endthreadex(0);
	return(0);
}

//tarefa de exibicao de dados do processo
void *Exibicao(void *arg) {
	char msg[100] = "";
	char nseq[7] = "";
	char hora[9] = "";
	char tz1[7] = "";
	char tz2[7] = "";
	char tz3[7] = "";
	char v[6] = "";
	char t[6] = "";
	char tempo[5] = "";
	int i, status, tipo;
	HANDLE waitExibeDados[2] = { ESCEve, ExibeDadosSem };
	HANDLE waitOcupados[2]{ ESCEve, hOcupadas2 };

	do {
		status = WaitForMultipleObjects(2, waitExibeDados, FALSE, INFINITE); // sincroniza com o teclado
		tipo = status - WAIT_OBJECT_0;
		if (tipo == 0)
			break;
		else {
			//checa se existem mensagens no buffer
			status = WaitForMultipleObjects(2, waitOcupados, FALSE, INFINITE);
			status = 
			tipo = status - WAIT_OBJECT_0;
			if (tipo == 0)
				break;
			else {
				//Formata as mensagens
				for (i = 0; i < 6; i++)
					nseq[i] = lista_circular2[pos_retirada2][i];

				for (i = 0; i < 8; i++)
					hora[i] = lista_circular2[pos_retirada2][i + 45];

				for (i = 0; i < 6; i++)
					tz1[i] = lista_circular2[pos_retirada2][i + 7];

				for (i = 0; i < 6; i++)
					tz2[i] = lista_circular2[pos_retirada2][i + 14];

				for (i = 0; i < 6; i++)
					tz3[i] = lista_circular2[pos_retirada2][i + 21];

				for (i = 0; i < 5; i++)
					v[i] = lista_circular2[pos_retirada2][i + 28];

				for (i = 0; i < 5; i++)
					t[i] = lista_circular2[pos_retirada2][i + 34];

				for (i = 0; i < 4; i++)
					tempo[i] = lista_circular2[pos_retirada2][i + 40];

				sprintf_s(msg, "NSEQ:%s %s TZ1:%s TZ2:%s TZ3:%s V:%s T:%s TEMPO:%s", nseq, hora, tz1, tz2, tz3, v, t, tempo);

				printf("%s\n", msg);

				pos_retirada2 = (pos_retirada2 + 1) % n2;
				ReleaseSemaphore(hVazias2, 1, NULL);
				ReleaseSemaphore(ExibeDadosSem, 1, NULL);
			}
		}
		
	} while (true); // Esc foi escolhido
	
	printf("Thread de exibicao de dados do processo sendo encerrada\n");
	_endthreadex(0);
	return(0);
}



